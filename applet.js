const Applet = imports.ui.applet;
const Util = imports.misc.util;
const Lang = imports.lang;
const MainLoop = imports.mainloop;

function SeasonalApplet(metadata, orientation, panel_height, instance_id) {
	this._init(metadata, orientation, panel_height, instance_id)
}

SeasonalApplet.prototype = {
	__proto__: Applet.TextApplet.prototype,
	_init: function(metadata, orientation, panel_height, instance_id) {
    Applet.TextApplet.prototype._init.call(this, orientation, panel_height, instance_id);
    this.updateLoop();
	},
	updateUI: function () {
		let details = SeasonalApplet.hours[new Date().getUTCHours()]
		this.set_applet_tooltip(details.longName)
		this.set_applet_label(details.emoji)
	},
	updateLoop: function () {
		this.updateUI();
		MainLoop.timeout_add_seconds(this.refreshInterval, Lang.bind(this, this.updateLoop))
	}
}

SeasonalApplet.hours = [
  {
    season: "winter",
    shortName: "candle",
    longName: "candle hour",
    emoji: "🕯"
  },
  {
    season: "winter",
    shortName: "ice",
    longName: "hour of ice",
    emoji: "❄️"
  },
  {
    season: "winter",
    shortName: "comet",
    longName: "hour of the comet",
    emoji: "☄️"
  },
  {
  	season: "winter",
  	shortName: "owl",
  	longName: "owl hour",
  	emoji: "🦉"
  },
  {
    season: "winter",
    shortName: "yarn",
    longName: "yarn hour",
    emoji: "🧶"
  },
  {
    season: "winter",
    shortName: "mist",
    longName: "hour of mist",
    emoji: "🌫"
  },
  {
    season: "spring",
    shortName: "sprout",
    longName: "sprout hour",
    emoji: "🌱"
  },
  {
    season: "spring",
    shortName: "rainbow",
    longName: "rainbow hour",
    emoji: "🌈"
  },
  {
    season: "spring",
    shortName: "worm",
    longName: "worm hour",
    emoji: "🪱"
  },
  {
    season: "spring",
    shortName: "rabbit",
    longName: "rabbit hour",
    emoji: "🐇"
  },
  {
    season: "spring",
    shortName: "blossom",
    longName: "blossom hour",
    emoji: "🌸"
  },
  {
    season: "spring",
    shortName: "nest",
    longName: "nest hour",
    emoji: "🪺"
  },
  {
    season: "summer",
    shortName: "coral",
    longName: "coral hour",
    emoji: "🪸"
  },
  {
    season: "summer",
    shortName: "cherry",
    longName: "cherry hour",
    emoji: "🍒"
  },
  {
  	season: "summer",
  	shortName: "bee",
  	longName: "bee hour",
  	emoji: "🐝"
  },
  {
    season: "summer",
    shortName: "melon",
    longName: "melon hour",
    emoji: "🍉"
  },
  {
    season: "summer",
    shortName: "seashell",
    longName: "seashell hour",
    emoji: "🐚"
  },
  {
    season: "summer",
    shortName: "dragon",
    longName: "hour of the dragon",
    emoji: "🐉"
  },
  {
    season: "autumn",
    shortName: "chestnut",
    longName: "chestnut hour",
    emoji: "🌰"
  },
  {
    season: "autumn",
    shortName: "kite",
    longName: "hour of the kite",
    emoji: "🪁"
  },
  {
    season: "autumn",
    shortName: "mushroom",
    longName: "mushroom hour",
    emoji: "🍄"
  },
  {
    season: "autumn",
    shortName: "lightning",
    longName: "lightning hour",
    emoji: "⚡️"
  },
  {
    season: "autumn",
    shortName: "mountain",
    longName: "hour of the mountain",
    emoji: "⛰"
  },
  {
    season: "autumn",
    shortName: "lantern",
    longName: "lantern hour",
    emoji: "🏮"
  }
]

SeasonalApplet.hourOf = function(when) {
	SeasonalApplet.hours(when.getUTCHours())
}

function main(metadata, orientation, panel_height, instance_id) {
  return new SeasonalApplet(metadata, orientation, panel_height, instance_id);
}


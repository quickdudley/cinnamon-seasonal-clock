# Seasonal Clock panel applet for Cinnamon

See https://seasonalclock.org

A certain group of friends decided on a name for each hour of the day,
consistently across all time zones. We sometimes use these to schedule
real-time chats and video calls.

This applet displays the appropriate emoji for the current hour on the Cinnamon
panel.

To install: clone this repo to `${HOME}.local/share/cinnamon/applets/seasonal-clock@quickdudley.github.io`

